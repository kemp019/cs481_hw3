﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HW3hierarch
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page3 : ContentPage
    {
        public Page3()
        {
            InitializeComponent();
        }

        //turns the title text red because the snake's eyes are red and glowing. So it probably tints everything red. Immersive.
        protected override void OnAppearing()
        { ((NavigationPage)Application.Current.MainPage).BarTextColor = Color.Red; }

        //grants you a memento of a golden soul, and tells you about it.
        protected override void OnDisappearing()
        {
            ((NavigationPage)Application.Current.MainPage).BarTextColor = Color.Black;
            DisplayAlert("A Strange Happening!", "You notice that the title text has turned black like the spider, likely so you will never forget him nor his wisdom.", "Very Cool");
        }
        //on appear and on disappear. +10 points

        //back we go
        async void ReturnStartClicked(object sender, EventArgs args)
        { await Navigation.PopToRootAsync(); }
    }
}