﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HW3hierarch
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page2 : ContentPage
    {
        public Page2()
        {
            InitializeComponent();
        }


        //turns the title bar permanently green because snakes are green and this way it will always remind you that you got killed by a snake.
        protected override void OnAppearing()
        { ((NavigationPage)Application.Current.MainPage).BarBackgroundColor = Color.ForestGreen; }

        //lets you know that you are dead.
        protected override void OnDisappearing()
        { DisplayAlert("You Have Died!", "One way or another, you have found your way to a premature end, carried to the afterlife upon the laughter of thirsting gods. Sucks, I guess.", "RIP"); }

        //on appear and on disappear. +10 points

        //back we go
        async void ReturnStartClicked(object sender, EventArgs args)
        { await Navigation.PopToRootAsync(); }

        void TodsPrisonClicked(object sender, EventArgs args)
        {
            Title = "TESV 3: Tod's Return";
        }
    }
}