﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HW3hierarch
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page4 : ContentPage
    {
        public Page4()
        {
            InitializeComponent();
        }

        //turns the background blue, since you will be fightign out of the blue corner. It will also strain your eyes, which will help make them stronger. Just like muscles.
        protected override void OnAppearing()
        { ((NavigationPage)Application.Current.MainPage).BackgroundColor = Color.LightBlue; }

        //grants you a memento of victory
        protected override void OnDisappearing()
        {
            ((NavigationPage)Application.Current.MainPage).BarTextColor = Color.White;
            ((NavigationPage)Application.Current.MainPage).BackgroundColor = Color.Gold;
            ((NavigationPage)Application.Current.MainPage).BarBackgroundColor = Color.Gold;
            DisplayAlert("The Aspect of a Champion!", "Ever since you beat the snake and won the championship, the reflection of your championship belt paints everything in gold.", "I'm a winner!");
        }
        //on appear and on disappear. +10 points

        //back we go
        async void ReturnStartClicked(object sender, EventArgs args)
        { await Navigation.PopToRootAsync(); }
    }
}