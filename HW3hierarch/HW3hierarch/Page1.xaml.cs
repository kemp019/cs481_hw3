﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HW3hierarch
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
        public Page1()
        {
            InitializeComponent();
        }
        public void OnClearClicked(object sender, EventArgs args)
        { }

        async void Page2Clicked(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new Page2()); //goes to page2
        }
        async void Page3Clicked(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new Page3()); //goes to page3
        }
        async void Page4Clicked(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new Page4()); //goes to page4
        }
    }
}